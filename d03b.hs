#!/usr/bin/env runhaskell

import Data.Char (isDigit)
import Data.List (splitAt)

partNumber :: String -> Int -> (Int, Int)
partNumber line j = (read $ (reverse $ takeWhile isDigit $ reverse a) ++ c, length c)
    where (a, b) = splitAt j line
          c = takeWhile isDigit b

gearRatio :: [String] -> Int -> Int -> Int -> Int -> Int -> Int -> [Int] -> [Int]
gearRatio grid w h i0 j0 i j nums
    | i < imin = gearRatio grid w h i0 j0 imin j nums
    | j < jmin = gearRatio grid w h i0 j0 i jmin nums
    | j >= min w (j0 + 2) = gearRatio grid w h i0 j0 (i + 1) jmin nums
    | i >= min h (i0 + 2) = nums
    | isDigit el = gearRatio grid w h i0 j0 i (j + skip) (nums ++ [num])
    | otherwise = gearRatio grid w h i0 j0 i (j + 1) nums
    where row = grid !! i
          el = row !! j
          imin = max 0 $ i0 - 1
          jmin = max 0 $ j0 - 1
          (num, skip) = partNumber row j
    

findGears :: [String] -> Int -> Int -> Int -> Int -> [[Int]] -> [[Int]]
findGears grid w h i j gearRatios
  | j == w = findGears grid w h (i+1) 0 gearRatios
  | i == h = gearRatios
  | a == '*' = findGears grid w h i (j+1) (gearRatios ++ [gearRatio grid w h i j (i-1) (j-1) []])
  | otherwise = findGears grid w h i (j+1) gearRatios
    where a = grid !! i !! j

main :: IO ()
main = do
    args <- getContents
    let grid = lines args
    let w = length $ grid !! 0
    let h = length grid
    print $ sum $ map product $ filter (\x -> length x == 2) $ findGears grid w h 0 0 []
