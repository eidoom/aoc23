#!/usr/bin/env runhaskell

parse :: String -> [[Int]]
parse = map (map read . words) . lines

stepDiff :: [Int] -> [Int]
stepDiff [a,b] = [b-a]
stepDiff (a:b:rest) = b-a : stepDiff (b:rest)

repeatToZero :: [Int] -> [[Int]]
repeatToZero history
  | all (==0) history = [history]
  | otherwise = history : (repeatToZero $ stepDiff history)

addZero :: [[Int]] -> [[Int]]
addZero (first:rest) = (0 : first) : rest

extend :: [Int] -> [Int] -> Int
extend a b = head b - head a

extrapolate :: [[Int]] -> [[Int]]
extrapolate [pen, ult] = [pen, extend pen ult : ult]
extrapolate (first:second:rest) = first : (extrapolate ((extend first second : second) : rest))

main :: IO ()
main = getContents >>= print . sum . map (last . map head . extrapolate . addZero . reverse) . map repeatToZero . parse
