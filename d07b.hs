#!/usr/bin/env runhaskell

import Data.List (transpose, sortOn)
import qualified Data.Map as M

type Hist = [(Char, Int)]
type Char2Int = M.Map Char Int

parse :: String -> ([String], [Int])
parse s = (hands, map read bids)
    where [hands, bids] = transpose . map words . lines $ s

strength :: Char2Int = M.fromList $ zip (reverse "AKQT98765432J") [0..]
n :: Int = length strength

histogram :: String -> Hist
histogram s =
    reverse
    $ sortOn snd
    $ sortOn (\ (c, _) -> strength M.! c)
    $ M.toAscList
    $ M.fromListWith (+) (map (\ x -> (x, 1)) s)

kind :: Hist -> Int
kind hand = case hand of
              -- five of a kind
              [(_, 5)] -> 6
              [(_, 4), ('J', 1)] -> 6
              [(_, 3), ('J', 2)] -> 6
              [('J', 3), (_, 2)] -> 6
              [('J', 4), (_, 1)] -> 6
              -- four of a kind
              [(_, 4), (_, 1)] -> 5
              [(_, 3), (_, 1), ('J', 1)] -> 5
              [(_, 2), ('J', 2), (_, 1)] -> 5
              [('J', 3), (_, 1), (_, 1)] -> 5
              -- full house
              [(_, 3), (_, 2)] -> 4
              [(_, 2), (_, 2), ('J', 1)] -> 4
              -- three of a kind
              [(_, 3), (_, 1), (_, 1)] -> 3
              [(_, 2), (_, 1), (_, 1), ('J', 1)] -> 3
              [('J', 2), (_, 1), (_, 1), (_, 1)] -> 3
              -- two pair
              [(_, 2), (_, 2), (_, 1)] -> 2
              [(_, 2), (_, 1), (_, 1), ('J', 1)] -> 2
              -- one pair
              [(_, 2), (_, 1), (_, 1), (_, 1)] -> 1
              [(_, 1), (_, 1), (_, 1), (_, 1), ('J', 1)] -> 1
              -- high card
              [(_, 1), (_, 1), (_, 1), (_, 1), (_, 1)] -> 0

tieBreak :: String -> Int
tieBreak s = sum $ map (\ (c, i) -> n^i * strength M.! c) (zip s [4,3..])

score :: String -> Int
score hand = ((*n^5) $ kind $ histogram hand) + tieBreak hand

winnings :: ([String], [Int]) -> Int
winnings (hands, bids) =
    sum
    $ map (\ (a, b) -> a * b)
    $ zip [1..]
    $ map snd
    $ sortOn fst
    $ zip (map score hands) bids

main :: IO ()
main = getContents >>= print . winnings . parse
