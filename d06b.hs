#!/usr/bin/env runhaskell

parse :: String -> [Int]
parse = map (read . concat . tail . words) . lines

margin :: [Int] -> Int
margin [t, d] = length $ filter (>d) $ map (\ v -> v * (t - v) ) [1..t-1]

main :: IO ()
main = getContents >>= print . margin . parse
