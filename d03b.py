#!/usr/bin/env python

import sys, operator


def ratio(d, im, jm, w, h):
    pns = []
    match = False
    for i in range(max(0, im - 1), min(im + 2, h)):
        for j in range(max(0, jm - 1), min(jm + 2, w)):
            if i == im and j == jm:
                match = False
                continue
            if match:
                if not d[i][j].isdigit():
                    match = False
                continue
            if d[i][j].isdigit():
                match = True
                s = j
                ns = s - 1
                while ns >= 0 and d[i][ns].isdigit():
                    s = ns
                    ns -= 1
                e = j
                while True:
                    if e == w or not d[i][e].isdigit():
                        break
                    e += 1
                pns.append(int(d[i][s:e]))
        match = False
    return operator.mul(*pns) if len(pns) == 2 else 0


if __name__ == "__main__":
    d = [l.strip() for l in sys.stdin.readlines()]
    w = len(d[0])
    h = len(d)
    print(
        sum(
            ratio(d, i, j, w, h)
            for i in range(h)
            for j in range(w)
            if d[i][j] == "*"
        )
    )
