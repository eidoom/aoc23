#!/usr/bin/env runhaskell

import Data.List (scanl)

type Coord = (Int, Int)
data Dir = U | D | L | R | None deriving (Show, Read, Eq)

toCoord :: Dir -> Coord
toCoord U = (-1, 0)
toCoord D = (1, 0)
toCoord L = (0, -1)
toCoord R = (0, 1)

infixl 6 .+
(.+) :: Coord -> Coord -> Coord
(i, j) .+ (k, l) = (i + k, j + l)

infixl 7 .*
(.*) :: Int -> Coord -> Coord
n .* (i, j) = (n * i, n * j)

toDir :: Char -> Dir
toDir '0' = R
toDir '1' = D
toDir '2' = L
toDir '3' = U

hex :: String -> (Dir, Int)
hex ['(', '#', a, b, c, d, e, f, ')'] = (toDir f, read $ "0x" <> [a, b, c, d, e])

parse :: String -> [(Dir, Int)]
parse = map (hex . last . words) . lines

walk :: [(Dir, Int)] -> [Coord]
walk = scanl (\acc (dir, num) -> acc .+ num .* toCoord dir) (0, 0)

-- https://en.wikipedia.org/wiki/Shoelace_formula - triangle formula
showlace :: [Coord] -> Int
showlace = showlace' 0
    where
        showlace' :: Int -> [Coord] -> Int
        showlace' n [_] = n `div` 2  -- since starting point appears again at end
        showlace' n ((i0, j0):c1@(i1, j1):rest) = showlace' (n + j0 * i1 - j1 * i0) $ c1 : rest

-- https://en.wikipedia.org/wiki/Pick%27s_theorem (cf Stokes' Theorem)
pick input =
    let 
        a = showlace $ walk input
        b = (sum $ map snd input)
    in
        a + b `div` 2 + 1

main :: IO ()
main = getContents >>= print . pick . parse
