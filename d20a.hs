#!/usr/bin/env runhaskell

-- import Debug.Trace (traceShow)

import Data.List (all)

import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M

import Data.Either (fromRight)

import Text.Parsec (parse, eof, try, (<|>))
import Text.Parsec.Char (endOfLine, letter, string, char)
import Text.Parsec.Combinator (endBy1, sepBy1, many1)
import Text.Parsec.String (Parser)

data Mod =
    Broadcaster { destinations :: [String] } |
    FlipFlop { destinations :: [String], state :: Bool } |
    Conjunction { destinations :: [String], states :: (Map String Bool) } |
    Output { destinations :: [String] }
  deriving Show

type ModMap = Map String Mod

parseFile :: Parser ModMap
parseFile = do
    cmds <- endBy1 parseCmd endOfLine
    eof
    return $ M.fromList cmds

parseDestMods :: Parser [String]
parseDestMods = sepBy1 parseWord $ string ", "

parseWord :: Parser String
parseWord = many1 letter

parseCmd :: Parser (String, Mod)
parseCmd = parseBroadcaster <|> parseFlipFlop <|> parseConjunction

parseBroadcaster :: Parser (String, Mod)
parseBroadcaster = do
    string "broadcaster -> "
    init <- parseDestMods
    return ("broadcaster", Broadcaster init)

parseFlipFlop :: Parser (String, Mod)
parseFlipFlop = do
    char '%'
    modName <- parseWord
    string " -> "
    destMods <- parseDestMods
    return (modName, FlipFlop destMods False)

parseConjunction :: Parser (String, Mod)
parseConjunction = do
    char '&'
    modName <- parseWord
    string " -> "
    destMods <- parseDestMods
    return (modName, Conjunction destMods M.empty)

-- set module state
modRx :: String -> Bool -> Mod -> Mod
modRx _ _ (Broadcaster dests) = (Broadcaster dests)
modRx _ pulse (FlipFlop dests state) = FlipFlop dests $
    if pulse then state else not state
modRx src pulse (Conjunction dests states) = Conjunction dests $
    M.insert src pulse states

-- get module output signal
modTx :: Mod -> Bool
modTx (Broadcaster _) = False
modTx (FlipFlop _ state) = state
modTx (Conjunction _ states) = not $ all id $ M.elems states

initState :: ModMap -> ModMap
initState mods = M.mapWithKey (\name mod -> case mod of
                                Conjunction dests _ -> Conjunction dests $
                                    M.fromList $
                                    map (\x -> (x, False)) $
                                    findInputs name mods
                                x -> x
                                ) mods

findInputs :: String -> ModMap -> [String]
findInputs modName = M.keys . M.filter (\mod -> modName `elem` destinations mod)

type HighLow = (Int, Int)

run :: ModMap -> HighLow -> (ModMap, HighLow)
run = run' [("button", False, "broadcaster")]

run' :: [(String, Bool, String)] -> ModMap -> HighLow -> (ModMap, HighLow)
run' [] mods hl = (mods, hl)
run' ((src, sig, dest) : rest) mods hl = let
    newMods = M.adjust (modRx src sig) dest mods
    newMod = M.findWithDefault (Output []) dest newMods
    newDests = if (terminate newMod sig) then [] else destinations newMod
    queue = rest ++ (map (\newDest -> (dest, modTx newMod, newDest)) newDests)
    in
    -- traceShow (src, sig, dest) $ 
    run' queue newMods $ hl .+ (if sig then (1, 0) else (0, 1))

(.+) :: HighLow -> HighLow -> HighLow
(a, b) .+ (c, d) = (a + c, b + d)

terminate :: Mod -> Bool -> Bool
terminate (FlipFlop _ _) True = True
terminate _ _ = False

count n mods = uncurry (*) $
    snd $
    foldr (\_ (curMods, curHL) -> run curMods curHL) (mods, (0, 0)) [1..n]

main :: IO ()
main = getContents >>= print . count 1000 . initState . fromRight M.empty . parse parseFile ""
