#!/usr/bin/env runhaskell

import Data.Char (isSpace)
import Data.List (break, dropWhile, dropWhileEnd)
import qualified Data.Set as S
import qualified Data.IntMap as IM

map2 :: (a -> b) -> (a, a) -> (b, b)
map2 f (a, b) = (f a, f b)

strip :: String -> String
strip = dropWhile isSpace . dropWhileEnd isSpace

breakAfter :: Char -> String -> (String, String)
breakAfter d s = map2 strip (a, tail b)
    where (a, b) = break (== d) s

doB :: [String] -> [Int]
doB = map (S.size . uncurry S.intersection . map2 (S.fromList . map (\x -> read x :: Int) . words) . breakAfter '|' . snd . breakAfter ':')

type Int2Int = IM.IntMap Int

enumerate :: [Int] -> Int2Int
enumerate = IM.fromList . zip [1..]

countCopies :: Int2Int -> Int2Int
countCopies matches = do
    let originals = IM.map (\ v -> 1 ) matches
    IM.foldlWithKey outer originals matches
    where outer :: Int2Int -> Int -> Int -> Int2Int 
          outer a k v = foldr (inner (a IM.! k)) a [k+1..k+v]
          inner :: Int -> Int -> Int2Int -> Int2Int
          inner n x = IM.insertWith (+) x n

main :: IO ()
main = getContents >>= print . sum . IM.elems . countCopies . enumerate . doB . lines
