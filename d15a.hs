#!/usr/bin/env runhaskell

import Data.Char (isSpace, ord)
import Data.List (dropWhileEnd, foldl')

splitOn :: Eq a => a -> [a] -> [[a]]
splitOn delimiter list = case break (== delimiter) list of
    (before, [])    -> [before]
    (before, after) -> before : splitOn delimiter (tail after)

stripEnd :: String -> String
stripEnd = dropWhileEnd isSpace

hash :: String -> Int
hash = foldl' (\acc c -> (`mod` 256) $ (* 17) $ (+ acc) $ ord c) 0

main :: IO ()
main = getContents >>= print . sum . map hash . splitOn ',' . stripEnd
