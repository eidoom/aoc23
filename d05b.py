#!/usr/bin/env python

from sys import stdin
from itertools import groupby


if __name__ == "__main__":
    d = [l.strip() for l in stdin.readlines()]

    seeds = [int(x) for x in d[0][7:].split()]
    seed_ranges = sorted(
        [((a := seeds[i]), a + seeds[i + 1] - 1) for i in range(0, len(seeds), 2)],
        key=lambda x: x[0],
    )
    print(seed_ranges)

    maps = [
        sorted([[int(x) for x in m.split()] for m in [*g][1:]], key=lambda x: x[1])
        for t, g in groupby(d[2:], key=lambda x: x != "")
        if t
    ]
    print(maps)
