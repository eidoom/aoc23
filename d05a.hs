#!/usr/bin/env runhaskell

import Data.List (null, uncons, drop)
import Data.Maybe (fromJust)

splitAfter :: Eq a => a -> [a] -> [[a]]
splitAfter delimiter list = case break (== delimiter) list of
    (before, [])    -> [before]
    (before, after) -> before : splitAfter delimiter (drop 2 after)

type AMap = (Int, Int, Int)

readMaps :: [String] -> [[AMap]]
readMaps = map (map (aMap . map read . words)) . filter (not . null) . splitAfter "" 
    where aMap :: [Int] -> AMap
          aMap [a, b, c] = (a, b, c)

doMap :: Int -> [AMap] -> Int
doMap seed [] = seed
doMap seed ((drs, srs, rl) : maps)
  | srs <= seed && seed < srs + rl = drs + seed - srs
  | otherwise = doMap seed maps

main :: IO ()
main = do
    args <- getContents
    let (a, b) = fromJust $ uncons $ lines args
    let seeds :: [Int] = map read $ words $ drop 7 a
    let maps = readMaps b
    print $ minimum $ map (\ seed -> foldl (\ acc aMap -> doMap acc aMap ) seed maps ) seeds
