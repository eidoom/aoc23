#!/usr/bin/env runhaskell

import Data.Char (isSpace, ord, isAlpha)
import Data.List (dropWhileEnd, foldl', findIndex)
import Data.Foldable (toList)
import qualified Data.Sequence as S

stripEnd :: String -> String
stripEnd = dropWhileEnd isSpace

splitOn :: Eq a => a -> [a] -> [[a]]
splitOn delimiter list = case break (== delimiter) list of
    (before, [])    -> [before]
    (before, after) -> before : splitOn delimiter (tail after)

hash :: String -> Int
hash = foldl' (\acc c -> (`mod` 256) $ (* 17) $ (+ acc) $ ord c) 0

step :: S.Seq [(String,Int)] -> String -> S.Seq [(String,Int)]
step boxes x = let
    (label, cmd) = break (not . isAlpha) x
    box = hash label
    f = read $ tail cmd
    in S.adjust' (\x -> case head cmd of
         '=' -> case findIndex (== label) (map fst x) of
                                   Just n -> take n x ++ [(label, f)] ++ drop (n + 1) x
                                   Nothing -> (label, f) : x
         '-' -> filter ((/= label) . fst) x
         ) box boxes

steps :: [String] -> S.Seq [(String,Int)]
steps = foldl' step . S.fromList $ replicate 256 []

times :: (Int, Int) -> Int
times (a, b) = a * b

lensPowers :: [(String, Int)] -> [Int]
lensPowers = map times . zip [1..] . reverse . map snd

boxPowers :: [[Int]] -> [[Int]]
boxPowers = map (\(i, lenses) -> map (*i) lenses) . zip [1..]

main :: IO ()
main = getContents >>= print
    . sum
    . concat
    . boxPowers
    . map lensPowers
    . toList
    . steps
    . splitOn ','
    . stripEnd
