#!/usr/bin/env runhaskell

import qualified Data.Map as M

type Tree = M.Map String (String, String)

parse :: String -> (String, Tree)
parse s = (all !! 0, M.fromList $ map line $ drop 2 all)
    where all = lines s
          line [a1, a2, a3, ' ', '=', ' ', '(', b1, b2, b3, ',', ' ', c1, c2, c3, ')'] =
              ([a1, a2, a3], ([b1, b2, b3], [c1, c2, c3]))

follow :: [Char] -> Tree -> String -> Int -> Int
follow route network pos i
    | pos == "ZZZ" = i
    | otherwise = follow route network (dir $ network M.! pos) (i + 1)
    where dir = case route !! (i `mod` length route) of
                  'L' -> fst
                  'R' -> snd

main :: IO ()
main = do
    stdin <- getContents 
    let (route, network) = parse stdin
    print $ follow route network "AAA" 0
