#!/usr/bin/env runhaskell

import Data.List (transpose, partition, sortOn)
import qualified Data.Map as M

join :: (String, String) -> String
join (a, b) = a <> b

times :: (Int, Int) -> Int
times (a, b) = a * b

col :: String -> [String]
col list = case break (== '#') list of 
             ("", "") -> []
             (a, "") -> [a]
             ("", b) -> [head b] : col (tail b)
             (a, b) -> (a <> [head b]) : col (tail b)

slide :: [String] -> [String]
slide = map (concat . map (join . partition (== 'O')) . col)

slideN :: [String] -> [String]
slideN = transpose . slide . transpose

slideW :: [String] -> [String]
slideW = slide

slideS :: [String] -> [String]
slideS = reverse . transpose . slide . transpose . reverse

slideE :: [String] -> [String]
slideE = map reverse . slide . map reverse

tiltCycle :: [String] -> [String]
tiltCycle = slideE . slideS . slideW . slideN

type PMap = M.Map [String] Int

periodValues :: Int -> PMap -> [[String]]
periodValues offset = map fst . drop offset . sortOn snd . M.toAscList

findPeriod' :: Int -> PMap -> [String] -> (Int, Int, [[String]])
-- assume the first period will be repeated
findPeriod' n hashmap pattern = case M.lookup pattern hashmap of
    Just a -> (a, n - a, periodValues a hashmap)
    Nothing -> findPeriod' (n + 1) (M.insert pattern n hashmap) (tiltCycle pattern)

findPeriod :: [String] -> (Int, Int, [[String]])
findPeriod = findPeriod' 0 M.empty

getLast :: Int -> (Int, Int, [[String]]) -> [String]
getLast n (offset, period, values) = values !! (mod (n - offset) period)

getLoad :: [String] -> Int
getLoad rocks = sum
    $ map times
    $ zip (reverse $ map (length . filter (== 'O')) rocks) [1..]

main :: IO ()
main = getContents >>= print . getLoad . getLast 1000000000 . findPeriod . lines
