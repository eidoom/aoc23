#!/usr/bin/env runhaskell

import Data.List (all)

import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M

import Data.Either (fromRight)

import Text.Parsec (parse, eof, (<|>))
import Text.Parsec.Char (endOfLine, letter, string, char)
import Text.Parsec.Combinator (endBy1, sepBy1, many1)
import Text.Parsec.String (Parser)

data Mod =
    Broadcaster { destinations :: [String] } |
    FlipFlop { destinations :: [String], state :: Bool } |
    Conjunction { destinations :: [String], states :: (Map String Bool) }
  deriving Show

type ModMap = Map String Mod

parseFile :: Parser ModMap
parseFile = do
    cmds <- endBy1 parseCmd endOfLine
    eof
    return $ M.fromList cmds

parseDestMods :: Parser [String]
parseDestMods = sepBy1 parseWord $ string ", "

parseWord :: Parser String
parseWord = many1 letter

parseCmd :: Parser (String, Mod)
parseCmd = parseBroadcaster <|> parseFlipFlop <|> parseConjunction

parseBroadcaster :: Parser (String, Mod)
parseBroadcaster = do
    string "broadcaster -> "
    init <- parseDestMods
    return ("broadcaster", Broadcaster init)

parseFlipFlop :: Parser (String, Mod)
parseFlipFlop = do
    char '%'
    modName <- parseWord
    string " -> "
    destMods <- parseDestMods
    return (modName, FlipFlop destMods False)

parseConjunction :: Parser (String, Mod)
parseConjunction = do
    char '&'
    modName <- parseWord
    string " -> "
    destMods <- parseDestMods
    return (modName, Conjunction destMods M.empty)

-- set module state
modRx :: String -> Bool -> Mod -> Mod
modRx _ _ (Broadcaster dests) = (Broadcaster dests)
modRx _ pulse (FlipFlop dests state) = FlipFlop dests $
    if pulse then state else not state
modRx src pulse (Conjunction dests states) = Conjunction dests $
    M.insert src pulse states

-- get module output signal
modTx :: Mod -> Bool
modTx (Broadcaster _) = False
modTx (FlipFlop _ state) = state
modTx (Conjunction _ states) = not $ all id $ M.elems states

initState :: ModMap -> ModMap
initState mods = M.mapWithKey (\name mod -> case mod of
                                Conjunction dests _ -> Conjunction dests $
                                    M.fromList $
                                    map (\x -> (x, False)) $
                                    findInputs name mods
                                x -> x
                              ) mods

findInputs :: String -> ModMap -> [String]
findInputs modName = M.keys . M.filter (\mod -> modName `elem` destinations mod)

-- we use a dict of the positions in button presses of the zh inputs being high
-- to easily keep track of them while running a single simulation
-- (turns out none of them cycle again before the slowest finishes its first cycle anyway)
type MatchDict = Map String [Int]

run' :: MatchDict -> String -> ModMap -> Int -> MatchDict
run' = run [("button", False, "broadcaster")]

run :: [(String, Bool, String)] -> MatchDict -> String -> ModMap -> Int -> MatchDict
run [] matches match mods num = run' matches match mods $ num + 1
run ((src, sig, dest) : rest) matches match mods num =
  if all ((>0) . length) $ M.elems matches
     then matches
     else let
        newMatches = if sig && dest == match
                        then M.insertWith (++) src [num] matches
                        else matches
        newMods = M.adjust (modRx src sig) dest mods
        newQueue = rest ++ case newMods M.!? dest of
                               Nothing -> []
                               Just newMod -> if terminate newMod sig
                                   then []
                                   else map (\newDest -> (dest, modTx newMod, newDest)) $
                                       destinations newMod
        in
        run newQueue newMatches match newMods num

terminate :: Mod -> Bool -> Bool
terminate (FlipFlop _ _) True = True
terminate _ _ = False

findPeriod :: String -> ModMap -> Int
findPeriod match mods =
    lcms $
    map head $
    M.elems $
    run'
        (M.fromList $ map (\x -> (x, [])) $ findInputs match mods)
        match
        mods
        -- don't forget to count the first button push!
        1

lcms :: [Int] -> Int
lcms xs = foldr lcm 1 xs

main :: IO ()
-- &zh -> rx is only input for rx
-- rx receives low when all zh inputs are high
-- zh has 4 inputs (sx, jt, kb, ks) which are all single-output conjunction modules (&)
-- and *their* input trees form 4 separate subgraphs,
-- each starting from one of the four outputs of broadcaster
-- let's assume the zh inputs are periodic (in number of button presses) and aligned on start
-- such that if we count the first instances of the zh inputs sending high
-- we can take the lowest common denominator of them to get when zh sends low (in number of *button presses*)
main = getContents >>= print . findPeriod "zh" . initState . fromRight M.empty . parse parseFile ""
