#!/usr/bin/env runhaskell

{-# LANGUAGE MultiWayIf #-}

import qualified Data.Map as M

findStart :: [[Char]] -> (Int, Int) -> (Int, Int)
findStart pipes (i, j)
    | j == w = findStart pipes (i + 1, 0)
    | e == 'S' = (i, j)
    | otherwise = findStart pipes (i, j + 1)
    where r = pipes !! i
          w = length r
          e = r !! j

findFork :: [[Char]] -> (Int, Int) -> ((Int, Int), Char)
findFork pipes (i, j)
  | i > 0 && (pipes !! (i - 1) !! j) `elem` ['|', 'F', '7'] = ((i-1, j), 'N')
  | j < jmax && (pipes !! i !! (j + 1)) `elem` ['-', '7', 'J'] = ((i, j+1), 'E')
  | i < imax && (pipes !! (i + 1) !! j) `elem` ['|', 'L', 'J'] = ((i+1, j), 'S')
  | j > 0 && (pipes !! i !! (j - 1)) `elem` ['-', 'F', 'L'] = ((i, j-1), 'W')
  where jmax = (length (pipes !! 0)) - 1
        imax = (length pipes) - 1

-- this time, remember the points which are pipes and mark the vertical-type pipes by direction: U (up) or D (down)
goRound :: [[Char]] -> (Int, Int) -> Char -> [((Int, Int), Char)] -> [((Int, Int), Char)]
goRound pipes p@(i, j) dir trits = case pipes !! i !! j of
      '-' -> case dir of
              'E' -> goRound pipes (i, j + 1) 'E' ((p,' ') : trits)
              'W' -> goRound pipes (i, j - 1) 'W' ((p,' ') : trits)
      '|' -> case dir of
              'S' -> goRound pipes (i + 1, j) 'S' ((p,'D') : trits)
              'N' -> goRound pipes (i - 1, j) 'N' ((p,'U') : trits)
      'L' -> case dir of
              'S' -> goRound pipes (i, j + 1) 'E' ((p,'D') : trits)
              'W' -> goRound pipes (i - 1, j) 'N' ((p,'U') : trits)
      'F' -> case dir of
              'N' -> goRound pipes (i, j + 1) 'E' ((p,'U') : trits)
              'W' -> goRound pipes (i + 1, j) 'S' ((p,'D') : trits)
      '7' -> case dir of
              'E' -> goRound pipes (i + 1, j) 'S' ((p,'D') : trits)
              'N' -> goRound pipes (i, j - 1) 'W' ((p,'U') : trits)
      'J' -> case dir of
              'E' -> goRound pipes (i - 1, j) 'N' ((p,'U') : trits)
              'S' -> goRound pipes (i, j - 1) 'W' ((p,'D') : trits)
      'S' -> (p, case dir of
                   'N' -> 'U' 
                   'S' -> 'D'
                   _ -> ' ') : trits

-- there must be a better way to implement this...
-- we have to count all points enclosed by the closed-loop pipe
-- scan over the lines (left to right), considering each independently
-- count non-pipe points between each pair of U/D pipes (as found sequentially)
-- next pair must start with the opposite direction,
-- so in ".UD-D.U-UD." (line 5 of t10c), the 2nd D is ignored
interior :: [[Char]] -> M.Map (Int, Int) Char -> (Int, Int) -> Bool -> Char -> Int -> Int -> Int
interior pipes trits p@(i, j) a c t n
  -- handle line ends
  | j == (length (pipes !! 0)) = interior pipes trits (i+1, 0) False ' ' 0 n
  -- handle input end
  | i == (length pipes) = n
  | otherwise = case trits M.!? p of
                  Just d -> if
                      -- point is a horizontal pipe
                      -- just move to next point
                      | d == ' ' -> interior pipes trits next a c t n
                      -- point is a U/D pipe, and has different U/D than last one
                      -- start (set t to 0, set a to active/True) or end (add t to n, reset t, set a to False) a pair
                      | d /= c -> interior pipes trits next (not a) d 0 (n + t)
                      -- point is a U/D pipe, but has same U/D as last one
                      -- reset the temporary register (t) to zero, it was counting external points
                      | d == c -> interior pipes trits next a d 0 n
                  -- point is not a pipe
                  -- count it in the temporary register
                  Nothing -> interior pipes trits next a c (if a then t + 1 else t) n
  where next = (i, j+1)

main :: IO ()
main = do
    args <- getContents 
    let pipes = lines args
    let start = findStart pipes (0, 0)
    let (fp, fd) = findFork pipes start
    let trits = M.fromList $ goRound pipes fp fd []
    print $ interior pipes trits (0, 0) False ' ' 0 0
