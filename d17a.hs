#!/usr/bin/env runhaskell

import Data.List ((\\))
import Data.Char (digitToInt)
import Data.Set (Set)
import qualified Data.Set as Set
import D17 (Heap, insert, pop, fromList)

type Coord = (Int, Int)
type State = (Coord, Dir, Int)

data Dir = N | E | S | W deriving (Show, Eq, Ord)

opposite :: Dir -> Dir
opposite N = S
opposite E = W
opposite S = N
opposite W = E

step :: Dir -> Coord
step N = (-1, 0)
step E = (0, 1)
step S = (1, 0)
step W = (0, -1)

(.+) :: Coord -> Coord -> Coord
(a, b) .+ (c, d) = (a + c, b + d)

-- A* optimise?
-- Data.Array for grid?
dijkstra :: [[Int]] -> Int
dijkstra grid = dijkstra' (fromList [(((0, 0), E, 0), 0), (((0, 0), S, 0), 0)]) Set.empty
  where
    dijkstra' :: Heap State -> Set State -> Int
    dijkstra' pq visited = 
      let
        w = length (grid !! 0)
        h = length grid
        inside (i, j) = and [j >= 0, j < w, i >= 0, i < h]
        (Just (state@(coord, dir, straight), loss), pq') = pop pq
        neighbours =
          filter
            (\(cur'@(coord', _, straight'), _) ->
              -- don't go off the edge
              inside coord'
              -- don't move more than 3 blocks in a single direction
              && straight' < 3
              -- don't repeat search State states
              && Set.notMember cur' visited
            ) $
          map (\dir' -> let coord'@(i, j) = coord .+ (step dir')
                         in ((coord', dir', if dir == dir' then straight + 1 else 0),
                             loss + grid !! i !! j)) $
          [N, E, S, W] \\ [opposite dir]
        newPQ = foldr (\(v, p) acc -> insert v p acc) pq' neighbours
        newVisited = Set.insert state visited
      in
        -- terminate if reach last block else recurse
        if coord == (h - 1, w - 1) then loss else dijkstra' newPQ newVisited

parse :: String -> [[Int]]
parse = map (map digitToInt) . lines

main :: IO ()
main = getContents >>= print . dijkstra . parse
