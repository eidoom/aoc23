#!/usr/bin/env runhaskell

import D01 (sumUp)

main :: IO ()
main = getContents >>= print . sumUp id
