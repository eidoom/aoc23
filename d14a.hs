#!/usr/bin/env runhaskell

import Data.List (transpose, partition)

join :: (String, String) -> String
join (a, b) = a <> b

times :: (Int, Int) -> Int
times (a, b) = a * b

col :: String -> [String]
col list = case break (== '#') list of 
             ("", "") -> []
             (a, "") -> [a]
             ("", b) -> [head b] : col (tail b)
             (a, b) -> (a <> [head b]) : col (tail b)

slide :: [String] -> [String]
slide = transpose . map (concat . map (join . partition (== 'O')) . col) . transpose

count :: [String] -> Int
count rocks = sum $ map times $ zip (reverse $ map (length . filter (== 'O')) rocks) [1..]

main :: IO ()
main = getContents >>= print . count . slide . lines
