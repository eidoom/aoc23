#!/usr/bin/env runhaskell

import Data.Char (digitToInt)
import Data.Set (Set)
import qualified Data.Set as Set
import D17 (Heap, insert, pop, fromList)

type Pos = ((Int, Int), Char, Int)

-- A* optimise? L1 distance to end heuristic is slower!
dijkstra :: [[Int]] -> Int
dijkstra grid = dijkstra' (fromList [(((0, 0), 'E', 0), 0), (((0, 0), 'S', 0), 0)]) Set.empty
  where
    dijkstra' :: Heap Pos -> Set Pos -> Int
    -- dijkstra' Empty visited = 0
    dijkstra' pq visited = 
      let
        w = length (grid !! 0)
        h = length grid
        (Just (current@(coord@(i, j), dir, straight), loss), pq') = pop pq
        neighbours =
          filter
            (\(cur'@((ii, jj), dir', straight'), _) ->
              -- don't go off the edge
              jj >= 0
              && jj < w
              && ii >= 0
              && ii < h
              -- don't reverse direction
              && (case dir of
                   'N' -> dir' /= 'S'
                   'E' -> dir' /= 'W'
                   'S' -> dir' /= 'N'
                   'W' -> dir' /= 'E')
              -- don't move more than 10 blocks in a single direction
              -- and don't turn until after a minimum of 4 blocks
              && if dir == dir' then straight' < 10 else straight > 2
              -- don't repeat search Pos states
              && not (Set.member cur' visited)
            ) $
          map (\(pos@(ii, jj), dir') ->
            ((pos, dir', if dir == dir' then straight + 1 else 0), loss + grid !! ii !! jj))
          [
            ((i, j + 1), 'E'),
            ((i, j - 1), 'W'),
            ((i + 1, j), 'S'),
            ((i - 1, j), 'N')
          ]
        newPQ = foldr (\(v, p) acc -> insert v p acc) pq' neighbours
        newVisited = Set.insert current visited
      in
        -- terminate if reach last block on at least 4th move in a straight line else recurse
        if coord == (h - 1, w - 1) && straight >= 3 then loss else dijkstra' newPQ newVisited

parse :: String -> [[Int]]
parse = map (map digitToInt) . lines

main :: IO ()
main = getContents >>= print . dijkstra . parse
