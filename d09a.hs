#!/usr/bin/env runhaskell

parse :: String -> [[Int]]
parse = map (map read . words) . lines

stepDiff :: [Int] -> [Int]
stepDiff [a,b] = [b-a]
stepDiff (a:b:rest) = b-a : stepDiff (b:rest)

repeatToZero :: [Int] -> [[Int]]
repeatToZero history
  | all (==0) history = [history]
  | otherwise = history : (repeatToZero $ stepDiff history)

addZero :: [[Int]] -> [[Int]]
addZero (first:rest) = (first ++ [0]) : rest

extend :: [Int] -> [Int] -> Int
extend a b = last b + last a

extrapolate :: [[Int]] -> [[Int]]
extrapolate [pen, ult] = [pen, ult ++ [extend pen ult]]
extrapolate (first:second:rest) = first : (extrapolate ((second ++ [extend first second]) : rest))

main :: IO ()
main = getContents >>= print . sum . map (last . map last . extrapolate . addZero . reverse) . map repeatToZero . parse
