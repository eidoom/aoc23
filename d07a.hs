#!/usr/bin/env runhaskell

import Data.List (transpose, sortOn)
import qualified Data.Map as M

type Hist = [(Char, Int)]
type Char2Int = M.Map Char Int

parse :: String -> ([String], [Int])
parse s = (hands, map read bids)
    where [hands, bids] = transpose . map words . lines $ s

histogram :: String -> Hist
histogram s = reverse $ sortOn snd $ M.toAscList $ M.fromListWith (+) (map (\ x -> (x, 1)) s)

kind :: Hist -> Int
kind hand = case hand of
              [(_, 5)] -> 6
              [(_, 4), (_, 1)] -> 5
              [(_, 3), (_, 2)] -> 4
              [(_, 3), (_, 1), (_, 1)] -> 3
              [(_, 2), (_, 2), (_, 1)] -> 2
              [(_, 2), (_, 1), (_, 1), (_, 1)] -> 1
              _ -> 0

strength :: Char2Int = M.fromList $ zip (reverse "AKQJT98765432") [0..]
n :: Int = length strength

tieBreak :: String -> Int
tieBreak s = sum $ map (\ (c, i) -> n^i * strength M.! c) (zip s [4,3..])

score :: String -> Int
score hand = ((*n^5) $ kind $ histogram hand) + tieBreak hand

winnings :: ([String], [Int]) -> Int
winnings (hands, bids) =
    sum
    $ map (\ (a, b) -> a * b)
    $ zip [1..]
    $ map snd
    $ sortOn fst
    $ zip (map score hands) bids

main :: IO ()
main = getContents >>= print . winnings . parse
