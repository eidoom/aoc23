#!/usr/bin/env runhaskell

import Data.Maybe (fromJust)
import Data.List (find)
import qualified Data.Set as S
import qualified Data.Array as A

type Pt = (Int, Int)
type Grid = A.Array Pt Char
type Pts = S.Set Pt

toArray :: [Char] -> Grid
toArray input = let
    ls = lines input
    h = length ls
    w = length $ ls !! 0
    in
    A.listArray ((0,0), (h-1,w-1)) $ concat ls

getStart :: Grid -> Pt
getStart = fst . fromJust . find ((=='S') . snd) . A.assocs

walk :: Grid -> [Pt] -> [Pt]
walk grid queue = S.toAscList $ walk' queue S.empty
    where
    walk' :: [Pt] -> Pts -> Pts
    walk' [] out = out
    walk' (pos : rest) out = walk' rest $ out `S.union` neighbours pos
    neighbours :: Pt -> Pts
    neighbours (i, j) = S.fromList [
        (ii, jj) |
            (ii, jj) <- [(i+1, j), (i-1, j), (i, j-1), (i, j+1)],
            ii >= i0,
            ii <= h,
            jj >= j0,
            jj <= w,
            grid A.! (ii, jj) /= '#'
        ]
    ((i0, j0), (h, w)) = A.bounds grid

plots :: Int -> String -> Int
plots n input = let
    grid = toArray input
    start = getStart grid
    in
    length $ foldr (\_ acc -> walk grid acc) [start] [1..n]

main :: IO ()
main = getContents >>= print . plots 64
