#!/usr/bin/env runhaskell

import Data.List (transpose)

type Doublet a = (a, a)

toDoublet :: [t] -> Doublet t
toDoublet [a, b] = (a, b)

parse :: String -> [Doublet Int]
parse = map toDoublet . transpose . map (map read . tail . words) . lines

distance :: Int -> Int -> Int
distance t v = v * (t - v)

margin :: Doublet Int -> Int
margin (t, d) = length $ filter (> d) $ map (distance t) [1 .. t - 1]

main :: IO ()
main = getContents >>= print . product . map margin . parse
