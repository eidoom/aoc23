#!/usr/bin/env runhaskell
-- could also have used flood fill https://en.wikipedia.org/wiki/Flood_fill
import Data.List (foldl', mapAccumL, lookup, sortOn, groupBy)

type Coord = (Int, Int)
data Dir = U | D | L | R | None deriving (Show, Read, Eq)

toCoord :: Dir -> Coord
toCoord U = (-1, 0)
toCoord D = (1, 0)
toCoord L = (0, -1)
toCoord R = (0, 1)

infixl 6 .+
(.+) :: Coord -> Coord -> Coord
(i, j) .+ (k, l) = (i + k, j + l)

parse :: String -> [(Dir, Int)]
parse = map ((\[a,b,_] -> (read a, read b)) . words) . lines

withNextDir :: [(Dir, Int)] -> [((Dir, Int), Dir)]
withNextDir []           = []
withNextDir [x]          = [(x, fst x)]
withNextDir (x1:x2:rest) = (x1, fst x2) : withNextDir (x2 : rest)

walk :: [(Dir, Int)] -> [(Coord, Dir)]
walk = snd
    . foldl' (\(cur, acc) ((dir, num), fDir) ->
        let
        dir' = if dir `elem` [U,D] then dir else fDir
        (next, app) = mapAccumL (\a _ -> (a .+ toCoord dir, (a .+ toCoord dir, dir'))) cur [1..num]
         in (next, acc ++ app))
        ((0, 0), [])
    . withNextDir

pairs :: [a] -> [(a, a)]
pairs []           = []
pairs (x1:x2:rest) = (x1, x2) : pairs rest

pairVolume :: ([Int],[Int]) -> Int
pairVolume (l, r) = length l + length r + minimum r - maximum l - 1

lineVolume :: [(Coord, Dir)] -> Int
lineVolume = sum
    . map pairVolume
    . pairs
    . map (map fst)
    . groupBy (\(_, a) (_, b) -> a == b)
    . sortOn fst
    . map (\((a, b), c)->(b, c))

range :: (Enum a, Ord a) => [a] -> [a]
range a = [minimum a .. maximum a]

volume :: [(Coord, Dir)] -> Int
volume terrain =
    let
        is = map (fst . fst) terrain
    in
        sum $ map (\i -> lineVolume $ filter ((==i) . fst . fst) terrain) $ range is

-- display :: [(Coord, Dir)] -> [String]
-- display border = map (\i ->
--     map (\j -> case lookup (i, j) border of
--                  Just d -> head $ show d
--                  Nothing -> '.')
--                  $ range $ map (snd . fst) border)
--     $ range $ map (fst . fst) border

-- print2d :: [String] -> IO ()
-- print2d = mapM_ putStrLn

main :: IO ()
main = getContents >>= print . volume . walk . parse
-- main = getContents >>= print2d . display . walk . parse
