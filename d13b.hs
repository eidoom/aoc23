#!/usr/bin/env runhaskell

import Data.List (transpose, findIndex)

splitOn :: Eq a => a -> [a] -> [[a]]
splitOn delimiter list = case break (== delimiter) list of
    (before, [])    -> [before]
    (before, after) -> before : splitOn delimiter (tail after)

reduce :: (String, String) -> (String, String)
reduce (a, b) = (drop (la - length b) a, reverse $ take la b)
    where la = length a

findSmudge :: [String] -> Maybe Int
findSmudge = findIndex (== 1)
    . map (length . filter not . map (uncurry (==)))
    . transpose
    . map (\r -> map (\i -> reduce $ splitAt i r) [1..length r - 1])

rows :: [String] -> Int
rows pattern = case findSmudge pattern of
      Just n -> n + 1
      Nothing -> cols pattern

cols :: [String] -> Int
cols pattern = case findSmudge $ transpose pattern of
      Just n -> 100 * (n + 1)
      Nothing -> 0

main :: IO ()
main = getContents >>= print . sum . map rows . splitOn "" . lines
