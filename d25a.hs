#!/usr/bin/env runhaskell

import Data.Either (fromRight)
import qualified Data.Map.Strict as M
import qualified Data.Set as S
import Text.Parsec (parse, eof)
import Text.Parsec.Char (endOfLine, char, string, letter)
import Text.Parsec.Combinator (endBy1, sepBy1, count)
import Text.Parsec.String (Parser)

parseFile :: Parser [(String, [String])]
parseFile = parseLine `endBy1` endOfLine <* eof

parseLine :: Parser (String, [String])
parseLine = do
    node <- parseWord
    string ": "
    adjacent <- parseWord `sepBy1` char ' '
    return (node, adjacent)

parseWord :: Parser String
parseWord = count 3 letter

type Graph = M.Map String (S.Set String)

toGraph :: [(String, [String])] -> Graph
toGraph = foldr (\(node, adjs) acc -> let
    g = M.insertWith S.union node (S.fromList adjs) acc
    h = M.fromList $ map (\adj -> (adj, S.singleton node)) adjs
    in
    M.unionWith S.union g h
    ) M.empty

main :: IO ()
main = getContents >>= print . toGraph . fromRight [] . parse parseFile ""
