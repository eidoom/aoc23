#!/usr/bin/env runhaskell

import Data.List (transpose, elemIndices, null, findIndices)

findGalaxies :: [String] -> [(Integer, Integer)]
findGalaxies = concat
    . map (\(i, js) -> [(i, j) | j <- js])
    . filter (not . null . snd)
    . zip [0 ..]
    . map (map toInteger . elemIndices '#')

findBetweens :: [String] -> [Integer]
findBetweens = map toInteger . findIndices (all (== '.'))

expandRows :: Integer -> [Integer] -> [(Integer, Integer)] -> [(Integer, Integer)]
expandRows n betweens galaxies = foldr
    (\b acc -> map (\((i0, _), (i, j)) -> (if i0 > b then i + n - 1 else i, j)) $ zip galaxies acc)
    galaxies
    betweens

expandCols :: Integer -> [Integer] -> [(Integer, Integer)] -> [(Integer, Integer)]
expandCols n betweens galaxies = foldr
    (\b acc -> map (\((_, j0), (i, j)) -> (i, if j0 > b then j + n - 1 else j)) $ zip galaxies acc)
    galaxies
    betweens

expand :: Integer -> [Integer] -> [Integer] -> [(Integer, Integer)] -> [(Integer, Integer)]
expand n betweenRows betweenCols = expandCols n betweenCols . expandRows n betweenRows

pairs :: Ord a => [a] -> [(a,a)]
pairs xs = [(x, y) | x <- xs, y <- xs, x < y]

distance :: Num a => (a, a) -> (a, a) -> a
distance (i0, j0) (i1, j1) = abs (i1 - i0) + abs (j1 - j0)

main :: IO ()
main = do
    args <- getContents
    let universe = lines args
    let betweenRows = findBetweens universe
    let betweenCols = findBetweens $ transpose universe
    let galaxies = findGalaxies universe
    let expanded = expand 1000000 betweenRows betweenCols galaxies
    print $ sum $ map (uncurry distance) $ pairs expanded
