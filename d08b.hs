#!/usr/bin/env runhaskell

import qualified Data.Map as M

type Tree = M.Map String (String, String)

parse :: String -> (String, Tree)
parse s = (all !! 0, M.fromList $ map line $ drop 2 all)
    where all = lines s
          line [a1, a2, a3, ' ', '=', ' ', '(', b1, b2, b3, ',', ' ', c1, c2, c3, ')'] =
              ([a1, a2, a3], ([b1, b2, b3], [c1, c2, c3]))

follow :: [Char] -> Tree -> String -> Int -> Int
follow route network pos i = case pos of
    [_, _, 'Z'] -> i
    _ -> follow route network (dir $ network M.! pos) (i + 1)
    where dir = case route !! (i `mod` length route) of
                  'L' -> fst
                  'R' -> snd

lcms :: [Int] -> Int
lcms xs = foldr lcm 1 xs

main :: IO ()
main = do
    stdin <- getContents 
    let (route, network) = parse stdin
    let start = filter (\[_, _, e] -> e == 'A') $ M.keys network
    print $ lcms $ map (\ a -> follow route network a 0) start
