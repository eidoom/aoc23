#!/usr/bin/env runhaskell

import Data.List (transpose, findIndex)

splitOn :: Eq a => a -> [a] -> [[a]]
splitOn delimiter list = case break (== delimiter) list of
    (before, [])    -> [before]
    (before, after) -> before : splitOn delimiter (tail after)

reduce :: (String, String) -> (String, String)
reduce (a, b) = (drop (la - length b) a, reverse $ take la b)
    where la = length a

unwrap :: Num a => Maybe a -> a
unwrap m = case m of
    Just n -> (n + 1)
    Nothing -> 0

rows :: [String] -> Int
rows = unwrap
    . findIndex (== True)
    . map (all $ uncurry (==))
    . transpose
    . map (\r -> map (\i -> reduce $ splitAt i r) [1..length r - 1])

both :: [String] -> Int
both pattern = (rows pattern) + 100 * (rows $ transpose pattern)

main :: IO ()
main = getContents >>= print . sum . map both . splitOn "" . lines
