# [aoc23](https://gitlab.com/eidoom/aoc23)

Let's try Haskell for [this year](https://adventofcode.com/2023).

On Fedora,

```shell
sudo dnf install ghc ghc-split-devel graphviz
```

I consulted the books/guides/tutorials/resources:

* [Learn You a Haskell for Great Good!](http://learnyouahaskell.com/chapters)
* [Real World Haskell](https://book.realworldhaskell.org/read/)
* [A Gentle Introduction To Haskell](https://www.haskell.org/tutorial/index.html)

and also the reference manuals for the packages:

* [base](https://hackage.haskell.org/package/base/docs/index.html)
    * [Data.Char](https://hackage.haskell.org/package/base/docs/Data-Char.html)
    * [Data.List](https://hackage.haskell.org/package/base/docs/Data-List.html)
* [containers](https://hackage.haskell.org/package/containers) ([docs](https://haskell-containers.readthedocs.io))

Run like:
* interpreted
    ```sh
    ./d01a.hs < i01
    ```
* compiled
    ```sh
    ghc -O2 d01a.hs  # or make d01a
    ./d01a < i01
    ```

To limit memory usage when moving fast, eg to 8GB,
```sh
ulimit -v 8388608
```
