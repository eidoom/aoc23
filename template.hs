#!/usr/bin/env runhaskell

main :: IO ()
main = getContents >>= print . lines
