#!/usr/bin/env runhaskell

findStart :: [String] -> (Int, Int) -> (Int, Int)
findStart pipes (i, j)
    | j == w = findStart pipes (i + 1, 0)
    | e == 'S' = (i, j)
    | otherwise = findStart pipes (i, j + 1)
    where r = pipes !! i
          w = length r
          e = r !! j

findForks :: [String] -> (Int, Int) -> [((Int, Int),Char)]
findForks pipes (i, j) = map (\(p,d,_) -> (p,d)) $ filter (\((ii, jj), _, cs) ->
    ii >= 0 && ii < length pipes && jj >= 0 && jj < length (pipes !! 0) &&
        ((pipes !! ii !! jj) `elem` cs)
    ) $
    zip3
    [(i-1,j), (i,j+1), (i+1,j), (i,j-1)]
    ['N','E','S','W']
    [['|','F','7'], ['-','7','J'], ['|','L','J'], ['-','F','L']]

goRound :: [String] -> (Int, Int) -> Char -> Int -> Int
goRound pipes (i, j) dir n = case pipes !! i !! j of
  '-' -> goRound pipes (i, j + (if dir == 'E' then 1 else -1)) dir (n+1)
  '|' -> goRound pipes (i + (if dir == 'S' then 1 else -1), j) dir (n+1)
  'L' -> case dir of
          'S' -> goRound pipes (i,j+1) 'E' (n+1)
          'W' -> goRound pipes (i-1,j) 'N' (n+1)
  'F' -> case dir of
          'N' -> goRound pipes (i,j+1) 'E' (n+1)
          'W' -> goRound pipes (i+1,j) 'S' (n+1)
  '7' -> case dir of
          'E' -> goRound pipes (i+1,j) 'S' (n+1)
          'N' -> goRound pipes (i,j-1) 'W' (n+1)
  'J' -> case dir of
          'E' -> goRound pipes (i-1,j) 'N' (n+1)
          'S' -> goRound pipes (i,j-1) 'W' (n+1)
  'S' -> n

main :: IO ()
main = do
    args <- getContents 
    let pipes = lines args
    let start = findStart pipes (0, 0)
    -- closed loop so same distance either way round from arbitrary point
    -- so count loop length starting in arbitrary direction then divide by 2
    let (fp, fd) = head $ findForks pipes start
    print $ (`div` 2) $ goRound pipes fp fd 1
