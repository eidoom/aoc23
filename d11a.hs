#!/usr/bin/env runhaskell

import Data.List (transpose, elemIndices, null)

expandRow :: [String] -> [String]
expandRow = concat . map (\x -> if all (== '.') x then replicate 2 x else [x])

expandBoth :: [String] -> [String]
expandBoth = transpose . expandRow . transpose . expandRow

findGalaxies :: [String] -> [(Int, Int)]
findGalaxies = concat
    . map (\(i, js) -> [(i, j) | j <- js])
    . filter (not . null . snd)
    . zip [0 ..]
    . map (elemIndices '#')

pairs :: Ord a => [a] -> [(a,a)]
pairs xs = [(x, y) | x <- xs, y <- xs, x < y]

distance :: Num a => (a, a) -> (a, a) -> a
distance (i0, j0) (i1, j1) = abs (i1 - i0) + abs (j1 - j0)

main :: IO ()
main = getContents >>= print . sum . map (uncurry distance) . pairs . findGalaxies . expandBoth . lines
