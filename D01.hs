module D01 (sumUp) where

import Data.Char (isDigit)

doLine :: String -> Int
doLine = read . firstLast . filter isDigit
    where firstLast :: String -> String
          firstLast s = [head s, last s]

sumUp :: (String -> String) -> String -> Int
sumUp f = sum . map (doLine . f) . lines
