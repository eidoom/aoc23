#!/usr/bin/env sh

make -j$(nproc)

sol=(
	55090 54845
	2439 63711
	525911 75805607
	18619 8063216
	278755257 26829166
	505494 23632299
	251106089 249620106
	18727 18024643846273
	2101499000 1089
	7145 445
	10077850 504715068438
	7047 0
	35691 39037
	107053 88371
	514639 279470
	7562 7793
	698 825
	74074 112074045986829
	476889
)

i=0
for src in d*.hs; do
	if [ -x "$src" ]; then
		bin=${src%.hs}
		nam=${bin#?}
		inp=i${nam%?}
		ans=${sol[i]}
		i=$((i+1))

		t0=$(date +%s%N)

		gue=$(./$bin < $inp)

		t1=$(date +%s%N)

		d=$(( (t1 - t0) / 1000000 ))

		if [ $gue -eq $ans ]; then
			echo "$nam pass ($d ms)"
		else
			echo "$nam fail ($gue != $ans)"
		fi
	fi
done
