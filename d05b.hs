#!/usr/bin/env runhaskell

import Data.List (null, uncons, drop, sortOn, foldl')
import Data.Maybe (fromJust)

splitAfter :: Eq a => a -> [a] -> [[a]]
splitAfter delimiter list = case break (== delimiter) list of
    (before, []) -> [before]
    (before, delimiter : _ : after) -> before : splitAfter delimiter after

type Int3 = (Int, Int, Int)
type Int2 = (Int, Int)

readMaps :: [String] -> [[Int3]]
readMaps = map (sortOn snd3 . map (toMap . map read . words)) . filter (not . null) . splitAfter "" 
    where toMap :: [Int] -> Int3
          toMap [a, b, c] = (a, b, c)
          snd3 :: Int3 -> Int
          snd3 (_, s, _) = s

readSeeds :: String -> [Int2]
readSeeds = toPairs . map read . words . drop 7
    where toPairs :: [Int] -> [Int2]
          toPairs [] = []
          toPairs (x:y:rest) = (x, x+y-1) : toPairs rest

parse :: String -> ([Int2], [[Int3]])
parse s = (readSeeds hd, readMaps tl)
    where (hd, tl) = fromJust $ uncons $ lines s

step :: [Int2] -> [Int3] -> [Int2]
step ns [] = ns
step [] _ = []
step ns@(n@(a, b) : rn) ms@((dmin, smin, l) : rm)
  -- a and b in
  | ain && b <= smax = (a + diff, b + diff) : step rn ms
  -- a in, b above
  | ain && b > smax = (a + diff, dmax) : step ((smax + 1, b) : rn) rm
  -- a below, b in
  | a < smin && bin = (a, smin - 1) : (dmin, b + diff) : step rn ms
  -- a below, b above
  | a < smin && b > smax = (a, smin - 1) : (dmin, dmax) : step ((smax + 1, b) : rn) ms
  -- a and b below
  | b < smin = n : step rn ms
  -- a and b above
  | a > smax = step ns rm
      where smax = smin + l - 1
            dmax = dmin + l - 1
            diff = dmin - smin
            ain = a >= smin && a <= smax
            bin = b >= smin && b <= smax

-- this doesn't actually affect the runtime (of the binary compiled with optimisations)
-- but does make intermediate states easier to read for debugging
collapse :: [Int2] -> [Int2]
collapse [x] = [x]
collapse (x@(a, b) : y@(c, d) : rest)
  | c - b == 1 = collapse ((a, d) : rest)
  | otherwise = x : collapse (y : rest)

main :: IO ()
main = do
    args <- getContents
    let (seeds, maps) = parse args
    print $ minimum $ map fst $ foldl' (\acc mp -> step (collapse $ sortOn fst acc) mp) seeds maps
