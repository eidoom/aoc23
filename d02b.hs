#!/usr/bin/env runhaskell

import Data.List (drop, break)

splitAfter :: Eq a => a -> [a] -> [[a]]
splitAfter delimiter list = case break (== delimiter) list of
    (before, [])    -> [before]
    (before, after) -> before : splitAfter delimiter (drop 2 after)

pair :: String -> (String, Int)
pair s = (tail b, read a)
    where (a, b) = break (== ' ') s

stds :: (String, Int) -> (Int, Int, Int)
stds (col, n) = case col of
  "red" -> (n, 0, 0)
  "green" -> (0, n, 0)
  "blue" -> (0, 0, n)

toList :: ([Int], [Int], [Int]) -> [[Int]]
toList (a,b,c) = [a,b,c]

idf :: String -> Int
idf = read . drop 5

results :: String -> [(Int, Int, Int)]
results = map (foldl (\(a,b,c) (d,e,f) -> (a+d, b+e, c+f)) ((0,0,0)) . map (stds . pair) . splitAfter ',') . splitAfter ';'

game :: [String] -> (Int, Int)
game [a, b] = (idf a, product $ map maximum $ toList $ unzip3 $ results b)

toArrays :: String -> (Int, Int)
toArrays l = game $ splitAfter ':' l

solve :: [(Int, Int)] -> Int
solve = sum . map snd

main :: IO ()
main = getContents >>= print . solve . map toArrays . lines
