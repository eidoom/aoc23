#!/usr/bin/env runhaskell

import Data.List (sortBy)
import Data.Either (fromRight)
import Text.Parsec (parse, eof)
import Text.Parsec.Char (endOfLine, char, digit)
import Text.Parsec.Combinator (endBy1, sepBy1, many1)
import Text.Parsec.String (Parser)

type Pos = [Int]
type Brick = [Pos]

parseFile :: Parser [Brick]
parseFile = parseBrick `endBy1` endOfLine <* eof

parseBrick :: Parser Brick
parseBrick = parsePos `sepBy1` char '~'

parsePos :: Parser Pos
parsePos = (read <$> many1 digit) `sepBy1` char ','

zOrder :: [Brick] -> [Brick]
zOrder = sortBy (\[[_, _, a0], [_, _, a1]] [[_, _, b0], [_, _, b1]] -> compare (min a0 a1) (min b0 b1))

fall :: [Brick] -> [Brick]
fall bricks = fall' bricks []
    where
        fall' :: [Brick] -> [Brick] -> [Brick]
        fall' [] done = done
        fall' (cur : rest) done = fall' rest $ (move cur) : done
            where
                move :: Brick -> Brick
                move brick
                    -- | 
                    | otherwise = brick

main :: IO ()
main = getContents >>= print . fall . zOrder . fromRight [] . parse parseFile ""
