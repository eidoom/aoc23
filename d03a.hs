#!/usr/bin/env runhaskell

import Data.Char (isDigit, isAlphaNum)
import Data.List (null)

isSymbol :: Char -> Bool
isSymbol c = not ( isAlphaNum c || c == '.' )

check :: [String] -> Int -> Int -> Int -> Int -> Int -> Bool
check grid w h i0 j0 n = do
    let il = [max 0 $ i0 - 1 .. min (h - 1) (i0 + 1)]
    let jl = [max 0 $ j0 - (n + 1) .. min (w - 1) j0]
    let area = [ grid !! i !! j | i <- il, j <- jl ]
    not $ null $ filter isSymbol area

rec :: [String] -> Int -> Int -> Int -> Int -> String -> Int -> Int
rec grid w h i j b c
    | j == w = if pass
                  then rec grid w h (i+1) 0 "" (c + read b)
                  else rec grid w h (i+1) 0 "" c
    | i == h = c
    | isDigit a = rec grid w h i (j+1) (b ++ [a]) c
    | pass = rec grid w h i (j+1) "" (c + read b)
    | otherwise = rec grid w h i (j+1) "" c
    where a = grid !! i !! j
          pass = (not $ null b) && check grid w h i j (length b)

main :: IO ()
main = do
    args <- getContents
    let grid = lines args
    let w = length $ grid !! 0
    let h = length grid
    print $ rec grid w h 0 0 "" 0
