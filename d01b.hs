#!/usr/bin/env runhaskell

import Data.Char (intToDigit)
import Data.List (splitAt)
import D01 (sumUp)

nums :: [String]
nums = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]

trans :: Int -> String -> String
trans n s
  | s == [] = s
  | n == length nums = head s : (trans' $ tail s)
  | front == name = head name : (intToDigit $ n + 1) : trans' (last name : rest)
  | otherwise = trans (n + 1) s
  where (front, rest) = splitAt (length name) s
        name = nums !! n

trans' :: String -> String
trans' = trans 0

main :: IO ()
main = getContents >>= print . sumUp trans'
