#!/usr/bin/env runhaskell

import Data.Char (toUpper)
import Data.List (map, filter, sum)
import Data.Either (Either)

import Text.Parsec (parse, eof, (<|>), try)
import Text.Parsec.Char (endOfLine, char, letter, digit, oneOf)
import Text.Parsec.Combinator (endBy1, sepBy1, many1)
import Text.Parsec.Error (ParseError)
import Text.Parsec.String (Parser)

data Cat = X | M | A | S
    deriving (Show, Read)

data Rule = Branch {cat :: Cat, op :: Char, num :: Int, to :: String} | Term {reg :: String}
    deriving Show

type Workflow = [Rule]

idx :: Cat -> Int
idx X = 0
idx M = 1
idx A = 2
idx S = 3

parseFile :: Parser ([(String, Workflow)], [[Int]])
parseFile = do
    workflows <- endBy1 parseWorkflow endOfLine
    endOfLine
    cats <- endBy1 parseCats endOfLine
    eof
    return (workflows, cats)

word :: Parser String
word = many1 letter

number :: Parser Int
number = read <$> many1 digit

comma :: Parser Char
comma = char ','

category :: Parser Cat
category = read <$> (:[]) <$> toUpper <$> oneOf "xmas"

parseWorkflow :: Parser (String, Workflow)
parseWorkflow = do
  key <- word
  char '{'
  workflows <- sepBy1 (try parseBranch <|> Term <$> word) comma
  char '}'
  return (key, workflows)

parseBranch :: Parser Rule
parseBranch = do
  cat <- category
  op <- oneOf "<>"
  val <- number
  char ':'
  end <- word
  return $ Branch cat op val end

parseCats :: Parser [Int]
parseCats = do
    char '{'
    xmas <- sepBy1 parseCat comma
    char '}'
    if length xmas == 4 then return xmas else fail "category part numbers parsing failed"

parseCat :: Parser Int
parseCat = category >> char '=' >> number

doSeries :: String -> Either ParseError ([(String, Workflow)], [[Int]]) -> [Int]
doSeries start input = case input of
                      Right (workflows, catss) -> map sum $ filter (doSeries' workflows start) catss

doSeries' :: [(String, Workflow)] -> String -> [Int] -> Bool
doSeries' workflows register cats = let
    workflow = case lookup register workflows of
             Just w -> w
    nextRegister = doWorkflow workflow cats
    in case nextRegister of
        "R" -> False
        "A" -> True
        x -> doSeries' workflows x cats

doWorkflow :: Workflow -> [Int] -> String
doWorkflow (rule : rest) cats = case rule of
    Term reg -> reg
    Branch cat op num to -> let
        val = cats !! (idx cat)
        cond = case op of
                 '<' -> val < num
                 '>' -> val > num
        in if cond then to else doWorkflow rest cats

main :: IO ()
main = getContents >>= print . sum . doSeries "in" . parse parseFile ""
