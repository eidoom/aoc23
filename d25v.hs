#!/usr/bin/env runhaskell

import Data.Either (fromRight)
import Data.List (concat, intercalate, map)
import qualified Data.Map.Strict as M
import qualified Data.Set as S
import Text.Parsec (parse, eof)
import Text.Parsec.Char (endOfLine, char, string, letter)
import Text.Parsec.Combinator (endBy1, sepBy1, count)
import Text.Parsec.String (Parser)

parseFile :: Parser [(String, [String])]
parseFile = parseLine `endBy1` endOfLine <* eof

parseLine :: Parser (String, [String])
parseLine = do
    node <- parseWord
    string ": "
    adjacent <- parseWord `sepBy1` char ' '
    return (node, adjacent)

parseWord :: Parser String
parseWord = count 3 letter

main :: IO ()
main = do
    input <- getContents
    let graph = fromRight [] $ parse parseFile "" input

    putStrLn "strict graph {"
    putStrLn $ intercalate "\n" $ map (\(a, b) -> a ++ " -- {" ++ (intercalate " " b) ++ "}") graph
    putStrLn "}"
