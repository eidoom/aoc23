SRC = $(wildcard d*.hs)
BIN = $(basename $(SRC))

.PHONY: all

all: $(BIN)

d%: d%.hs
	ghc -O2 $< -o $@

clean:
	rm *.o *.hi $(BIN)
