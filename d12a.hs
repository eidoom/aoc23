#!/usr/bin/env runhaskell

import Data.List (break, group)

splitOn :: Eq a => a -> [a] -> [[a]]
splitOn delimiter list = case break (== delimiter) list of
    (before, [])    -> [before]
    (before, after) -> before : splitOn delimiter (tail after)

f :: [String] -> (String, [Int])
f [a, b] = (a, map read $ splitOn ',' b)

parse :: String -> [(String, [Int])]
parse = map (f . words) . lines

generateStrings :: String -> [String]
generateStrings [] = [""]
generateStrings ('?' : xs) = [c : rest | c <- ".#", rest <- generateStrings xs]
generateStrings (x : xs) = [x : rest | rest <- generateStrings xs]

possibilities :: String -> [[Int]]
possibilities = map (map length . filter (\x -> '#' == head x) . group) . generateStrings

main :: IO ()
main = getContents >>= print . sum . map (\(a, b) -> length $ filter (==b) $ possibilities a) . parse
