#!/usr/bin/env python

import sys


def isSymbol(b):
    return not (b.isalnum() or b == ".")


def check(d, im, jr, w, h, b):
    n = len(b)
    jl = jr - n - 1
    js = []
    if jr < w:
        js.append(jr)
    if jl >= 0:
        js.append(jl)
    for i in range(max(0, im - 1), min(im + 2, h)):
        if i == im:
            for j in js:
                if isSymbol(d[i][j]):
                    return True
        for j in range(max(0, jl), min(jr + 1, w)):
            if isSymbol(d[i][j]):
                return True
    return False


if __name__ == "__main__":
    d = [l.strip() for l in sys.stdin.readlines()]
    w = len(d[0])
    h = len(d)
    b = ""
    c = 0
    for i in range(h):
        for j in range(w):
            a = d[i][j]
            if a.isdigit():
                b += a
                if j != w - 1:
                    continue
            if b and check(d, i, j, w, h, b):
                c += int(b)
            b = ""

    print(c)
