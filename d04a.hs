#!/usr/bin/env runhaskell

import Data.Char (isSpace)
import Data.List (break, dropWhile, dropWhileEnd)
import qualified Data.Set as S

map2 :: (a -> b) -> (a, a) -> (b, b)
map2 f (a, b) = (f a, f b)

strip :: String -> String
strip = dropWhile isSpace . dropWhileEnd isSpace

breakAfter :: Char -> String -> (String, String)
breakAfter d s = map2 strip (a, tail b)
    where (a, b) = break (== d) s

main :: IO ()
main = getContents >>= print . sum . map ((2^) . subtract 1) . filter (>0) . map (S.size . uncurry S.intersection . map2 (S.fromList . map (\x -> read x :: Int) . words) . breakAfter '|' . snd . breakAfter ':') . lines
