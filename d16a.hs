#!/usr/bin/env runhaskell

import Data.Sequence (Seq (Empty, (:<|)), (><))
import qualified Data.Sequence as Seq
import Data.Set (Set)
import qualified Data.Set as Set

type Pos = ((Int, Int), Char)

bfs :: [String] -> Set Pos
bfs grid = bfs' (Seq.fromList [((0, 0), 'E')]) Set.empty
  where
    bfs' :: Seq Pos -> Set Pos -> Set Pos
    bfs' Empty visited = visited
    bfs' (current@((i, j), dir) :<| rest) visited
      | i == -1 || i == h || j == -1 || j == wd || Set.member current visited = bfs' rest visited
      | otherwise = bfs' (rest >< neighbours) (Set.insert current visited)
      where
        h = length grid
        wd = length $ grid !! 0
        neighbours = let
            n = ((i - 1, j), 'N')
            e = ((i, j + 1), 'E')
            s = ((i + 1, j), 'S')
            w = ((i, j - 1), 'W')
            in
            Seq.fromList $ case grid !! i !! j of
              '.' -> case dir of
                     'N' -> [n]
                     'E' -> [e]
                     'S' -> [s]
                     'W' -> [w]
              '/' -> case dir of
                     'N' -> [e]
                     'E' -> [n]
                     'S' -> [w]
                     'W' -> [s]
              '\\' -> case dir of
                     'N' -> [w]
                     'E' -> [s]
                     'S' -> [e]
                     'W' -> [n]
              '-' -> case dir of
                     'N' -> [e, w]
                     'E' -> [e]
                     'S' -> [e, w]
                     'W' -> [w]
              '|' -> case dir of
                     'N' -> [n]
                     'E' -> [n, s]
                     'S' -> [s]
                     'W' -> [n, s]

main :: IO ()
main = getContents >>= print . length . Set.map fst . bfs . lines
