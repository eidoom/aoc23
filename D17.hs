module D17 (
    Heap,
    singleton,
    insert,
    pop,
    fromList
   ) where

type Heap a = Heap' a Int

-- minimum binary heap as implementation of priority queue
data Heap' a b = Empty | Node a b (Heap' a b) (Heap' a b)
  deriving (Show, Read, Eq)

singleton :: a -> b -> Heap' a b
singleton value priority = Node value priority Empty Empty

merge :: (Eq a, Ord b) => Heap' a b -> Heap' a b -> Heap' a b
merge Empty h2 = h2
merge h1 Empty = h1
merge h1@(Node v1 p1 l1 r1) h2@(Node v2 p2 l2 r2)
  | v1 == v2  = insert v1 (min p1 p2) (merge (merge l1 r1) (merge l2 r2))
  | p1 <= p2  = Node v1 p1 l1 (merge r1 h2)
  | otherwise = Node v2 p2 l2 (merge h1 r2)

insert :: (Eq a, Ord b) => a -> b -> Heap' a b -> Heap' a b
insert v p Empty = singleton v p
insert value priority h = merge (singleton value priority) h

pop :: (Eq a, Ord b) => Heap' a b -> (Maybe (a, b), Heap' a b)
pop Empty = (Nothing, Empty)
pop (Node v p l r) = (Just (v, p), merge l r)

fromList :: (Eq a, Ord b) => [(a, b)] -> Heap' a b
fromList = foldr (\(v, p) h -> insert v p h) Empty
